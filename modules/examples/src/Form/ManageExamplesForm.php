<?php

namespace Drupal\breezy_layouts_examples\Form;

use Drupal\breezy_layouts_examples\ExamplesManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Examples management form.
 */
class ManageExamplesForm extends FormBase {

  use MessengerTrait;

  /**
   * Drupal\breezy_layouts_examples\ExamplesManagerInterface definition.
   *
   * @var \Drupal\breezy_layouts_examples\ExamplesManagerInterface
   */
  protected ExamplesManagerInterface $examplesManager;

  /**
   * Constructs a new ManageExamplesForm object.
   *
   * @param \Drupal\breezy_layouts_examples\ExamplesManagerInterface $examples_manager
   *   The examples manager service.
   */
  public function __construct(ExamplesManagerInterface $examples_manager) {
    $this->examplesManager = $examples_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\breezy_layouts_examples\ExamplesManagerInterface $examples_manager */
    $examples_manager = $container->get('breezy_layouts_examples.examples_manager');
    return new static($examples_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_examples_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm($form, FormStateInterface $form_state) {
    $utility_validation = $this->examplesManager->validateUtilityConfig();
    if (!$utility_validation) {
      $form['utility_validation'] = [
        '#markup' => '<div class="warning">' . $this->t('Utility requirements failed') . '</div>',
        '#allowed_tags' => ['div'],
      ];
      $form['utility_config_reset'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Reset Breezy Utility configuration'),
        '#description' => $this->t('Will overwrite your current settings.  Please backup and export config before performing this action.'),
      ];
    }
    else {
      $form['utility_validation'] = [
        '#markup' => '<div>' . $this->t('Utility settings passed') . '</div>',
        '#allowed_tags' => ['div'],
      ];
    }
    $layouts_validation = $this->examplesManager->validateLayoutsConfig();
    if (!$layouts_validation) {
      $form['layouts_validation'] = [
        '#markup' => '<div>' . $this->t('Layout settings passed') . '</div>',
        '#allowed_tags' => ['div'],
      ];
      $form['layouts_config_reset'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Reset Breezy Utility configuration'),
        '#description' => $this->t('Overwrites your current settings.  Please backup and export config before performing this action.'),
      ];
    }
    else {
      $form['layouts_validation'] = [
        '#markup' => '<div>' . $this->t('Layout settings passed') . '</div>',
        '#allowed_tags' => ['div'],
      ];
    }

    if ($utility_validation && $layouts_validation) {
      $config_rows = $this->buildRows($form, $form_state);
      $form['examples_config'] = [
        '#type' => 'tableselect',
        '#header' => [
          'file_name' => $this->t('File name'),
          'status' => $this->t('Status'),
        ],
        '#options' => $config_rows,
        '#empty' => $this->t('No examples found.'),
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $utility_config_reset = $form_state->getValue('utility_config_reset');
    if ($utility_config_reset) {
      $this->examplesManager->resetUtilityConfig();
      $this->messenger()->addStatus('Utility configuration reset');
    }
    $layouts_config_reset = $form_state->getValue('layouts_config_reset');
    if ($layouts_config_reset) {
      $this->examplesManager->resetLayoutsConfig();
      $this->messenger()->addStatus('Layouts configuration reset');
    }

    $examples = $form_state->getValue('examples_config');
    if ($examples) {
      $created = [];
      $updated = [];
      foreach ($examples as $example) {
        if (!$example) {
          continue;
        }
        $result = $this->examplesManager->importExample($example);
        $file_data = $this->examplesManager->readFile($example);
        $label = $file_data['label'];
        if ($result == $this->examplesManager::EXAMPLE_UPDATED) {
          $updated[] = $label;
        }
        else {
          $created[] = $label;
        }
      }
      foreach ($created as $value) {
        $this->messenger()->addStatus($this->t('Layout variant @example created', ['@example' => $value]));
      }
      foreach ($updated as $value) {
        $this->messenger()->addStatus($this->t('Layout variant @example updated', ['@example' => $value]));
      }
    }
    $form_state->setRebuild(FALSE);
  }

  /**
   * Build rows.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   An array of rows.
   */
  protected function buildRows(array $form, FormStateInterface $form_state) : array {
    $rows = [];
    $file_names = $this->examplesManager->listFiles();
    if (empty($file_names)) {
      return $rows;
    }
    foreach ($file_names as $name) {
      $file_data = $this->examplesManager->readFile($name);
      $status = $this->examplesManager->isActiveConfig($file_data);
      $rows[$name] = [
        'file_name' => $file_data['label'],
        'status' => $status ? $this->t('Installed') : $this->t('Not installed'),
      ];
    }
    return $rows;
  }

}
