<?php

namespace Drupal\breezy_layouts_examples;

/**
 * Provides an interface for ExamplesManager.
 */
interface ExamplesManagerInterface {

  /**
   * Finds files in the config/examples directory.
   *
   * @return array
   *   An array of config files.
   */
  public function listFiles() : array;

  /**
   * Reads a config file.
   *
   * @param string $name
   *   The name of the config file.
   *
   * @return array
   *   The decoded config.
   */
  public function readFile(string $name) : array;

  /**
   * Checks if a configuration item exists in the database.
   *
   * @param array $configuration
   *   The configuration to check.
   *
   * @return bool
   *   True if the configuration object exists.
   */
  public function isActiveConfig(array $configuration) : bool;

  /**
   * Process config.
   *
   * @param string $config_name
   *   The config name.
   *
   * @return string
   *   The updated or created string.
   */
  public function importExample(string $config_name) : string;

  /**
   * Get utility requirements.
   *
   * @return array
   *   The utility_requirements array.
   */
  public function getUtilityRequirements() : array;

  /**
   * Get layouts requirements.
   *
   * @return array
   *   The layouts requirements.
   */
  public function getLayoutsRequirement() : array;

  /**
   * Validates BreezyUtility settings.
   *
   * @return bool
   *   Whether the BreezyUtility settings are compatible.
   */
  public function validateUtilityConfig() : bool;

  /**
   * Reset utility config.
   *
   * @return void
   *   Returns nada.
   */
  public function resetUtilityConfig() : void;

  /**
   * Validates BreezyLayouts settings.
   *
   * @return bool
   *   Whether the BreezyLayouts settings are compatible.
   */
  public function validateLayoutsConfig() : bool;

  /**
   * Reset layouts config.
   *
   * @return void
   *   Returns nada.
   */
  public function resetLayoutsConfig() : void;

}
