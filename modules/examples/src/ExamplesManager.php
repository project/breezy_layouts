<?php

namespace Drupal\breezy_layouts_examples;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * Provides a manager for examples.
 */
class ExamplesManager implements ExamplesManagerInterface {

  /**
   * The BreezyUtility settings config.
   *
   * @var string
   */
  const BREEZY_UTILITY_SETTINGS = 'breezy_utility.settings';

  /**
   * The BreezyLayouts settings config.
   *
   * @var string
   */
  const BREEZY_LAYOUTS_SETTINGS = 'breezy_layouts.settings';

  /**
   * Import status 'created'.
   *
   * @var string
   */
  const EXAMPLE_CREATED = 'created';

  /**
   * Import status 'updated'.
   *
   * @var string
   */
  const EXAMPLE_UPDATED = 'updated';

  /**
   * Utility validation parameters.
   *
   * @var array
   */
  protected array $utilityValidation = [
    'utility_classes_group' => 'breezy_utility.tailwind',
    'breakpoint_group' => 'breezy_utility.tailwind',
    'breakpoints' => [
      'breezy_utility__tailwind__mobile' => ['prefix' => ''],
      'breezy_utility__tailwind__sm' => ['prefix' => 'sm:'],
      'breezy_utility__tailwind__md' => ['prefix' => 'md:'],
      'breezy_utility__tailwind__lg' => ['prefix' => 'lg:'],
      'breezy_utility__tailwind__xl' => ['prefix' => 'xl:'],
      'breezy_utility__tailwind__2xl' => ['prefix' => '2xl:'],
    ],
  ];

  /**
   * Layouts validation parameters.
   *
   * @var array
   */
  protected array $layoutsValidation = [
    'layouts' => [
      'breezy-five-column',
      'breezy-four-column',
      'breezy-one-column',
      'breezy-three-column',
      'breezy-two-column',
    ],
  ];

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected ConfigManagerInterface $configManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * File storage.
   *
   * @var \Drupal\Core\Config\FileStorage
   */
  protected FileStorage $fileStorage;

  /**
   * Constructs a new ExamplesManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The config manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list
   *   The module extension list.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, ConfigManagerInterface $config_manager, ModuleExtensionList $extension_list, LoggerChannelFactoryInterface $logger_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->configManager = $config_manager;
    $this->logger = $logger_factory->get('breezy_layouts_examples');

    $path_to_config = $extension_list->getPath('breezy_layouts_examples') . '/config/examples/';
    $this->fileStorage = new FileStorage($path_to_config);
  }

  /**
   * {@inheritdoc}
   */
  public function listFiles() : array {
    return $this->fileStorage->listAll('breezy_layouts');
  }

  /**
   * {@inheritdoc}
   */
  public function readFile(string $name) : array {
    return $this->fileStorage->read($name);
  }

  /**
   * {@inheritdoc}
   */
  public function isActiveConfig(array $configuration) : bool {
    if (isset($configuration['id'])) {
      return (bool) $this->entityTypeManager->getStorage('breezy_layouts_variant')->load($configuration['id']);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function importExample(string $config_name) : string {
    $values = $this->readFile($config_name);
    $id = $values['id'];
    /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('breezy_layouts_variant');
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $entity */
    $entity = $storage->load($id);

    if ($entity) {
      $entity = $storage->updateFromStorageRecord($entity, $values);
      $entity->save();
      return static::EXAMPLE_UPDATED;
    }
    else {
      $entity = $storage->createFromStorageRecord($values);
      $entity->save();
      return static::EXAMPLE_CREATED;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getUtilityRequirements() : array {
    return $this->utilityValidation;
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutsRequirement() : array {
    return $this->layoutsValidation;
  }

  /**
   * {@inheritdoc}
   */
  public function validateUtilityConfig() : bool {
    $utility_config = $this->configFactory->get(static::BREEZY_UTILITY_SETTINGS);
    foreach ($this->utilityValidation as $config_name => $config_value) {
      if ($config_value !== $utility_config->get($config_name)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function resetUtilityConfig() : void {
    $utility_config = $this->configFactory->getEditable(static::BREEZY_UTILITY_SETTINGS);
    foreach ($this->utilityValidation as $config_name => $config_value) {
      $utility_config->set($config_name, $config_value);
    }
    $utility_config->save();
  }

  /**
   * {@inheritdoc}
   */
  public function validateLayoutsConfig() : bool {
    $layouts_config = $this->configFactory->get(static::BREEZY_LAYOUTS_SETTINGS);
    foreach ($this->layoutsValidation as $config_name => $config_value) {
      if ($config_value !== $layouts_config->get($config_name)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function resetLayoutsConfig() : void {
    $layouts_config = $this->configFactory->getEditable(static::BREEZY_LAYOUTS_SETTINGS);
    foreach ($this->layoutsValidation as $config_name => $config_value) {
      $layouts_config->set($config_name, $config_value);
    }
    $layouts_config->save();
  }

}
