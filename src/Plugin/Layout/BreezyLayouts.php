<?php

namespace Drupal\breezy_layouts\Plugin\Layout;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\breezy_layouts\Service\VariantManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Layout class for Breezy Layouts.
 */
class BreezyLayouts extends LayoutDefault implements ContainerFactoryPluginInterface {

  /**
   * Config Breakpoints.
   *
   * @var array
   */
  protected array $breakpoints;

  /**
   * Drupal\breakpoint\BreakpointManagerInterface definition.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected BreakpointManagerInterface $breakpointManager;

  /**
   * Drupal\breezy_layouts\Service\VariantManagerInterface definition.
   *
   * @var \Drupal\breezy_layouts\Service\VariantManagerInterface
   */
  protected VariantManagerInterface $variantManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager
   *   The breakpoint manager service.
   * @param \Drupal\breezy_layouts\Service\VariantManagerInterface $variant_manager
   *   The variant manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BreakpointManagerInterface $breakpoint_manager, VariantManagerInterface $variant_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->breakpointManager = $breakpoint_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->variantManager = $variant_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager */
    $breakpoint_manager = $container->get('breakpoint.manager');
    /** @var \Drupal\breezy_layouts\Service\VariantManagerInterface $variant_manager */
    $variant_manager = $container->get('breezy_layouts.variant.manager');
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $breakpoint_manager,
      $variant_manager,
      $entity_type_manager
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'variant' => NULL,
      'variant_settings' => [],
      'classes' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $layout_definition = $this->getPluginDefinition();
    $configuration = $this->getConfiguration();
    $variant_options = $this->variantManager->getVariantOptionsForLayout($layout_definition->get('id'));
    if (empty($variant_options)) {
      return $form;
    }

    $variant = $form_state->get('variant');
    $input = $form_state->getUserInput();
    // Layout paragraphs structure is different than layout builder.
    if (isset($input['layout_settings']['variant'])) {
      $variant = $input['layout_settings']['variant'];
    }
    elseif (isset($input['layout_paragraphs']['config']['variant'])) {
      $variant = $input['layout_paragraphs']['config']['variant'];
    }
    elseif (!empty($configuration['variant'])) {
      $variant = $configuration['variant'];
    }

    $variant_wrapper_id = 'variant-settings-wrapper';
    $form['variant'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose variant'),
      '#options' => $variant_options,
      '#default_value' => $variant ?? '',
      '#empty_option' => $this->t('-- Select --'),
      '#ajax' => [
        'callback' => [$this, 'variantSelectCallback'],
        'wrapper' => $variant_wrapper_id,
      ],
    ];

    $form['variant_settings_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $variant_wrapper_id,
      ],
    ];
    $variant_entity = NULL;
    if ($variant) {
      /** @var \Drupal\breezy_layouts\Entity\BreezyLayoutsVariantInterface $variant_entity */
      $variant_entity = $this->entityTypeManager->getStorage('breezy_layouts_variant')->load($variant);
    }
    if ($variant_entity) {
      // Layout paragraphs structure is different than layout builder.
      if (isset($input['layout_settings']['variant_settings_wrapper']['variant_settings'])) {
        $variant_settings = $input['layout_settings']['variant_settings_wrapper']['variant_settings'];
      }
      elseif (isset($input['layout_paragraphs']['config']['variant_settings_wrapper']['variant_settings'])) {
        $variant_settings = $input['layout_paragraphs']['config']['variant_settings_wrapper']['variant_settings'];
      }
      else {
        $variant_settings = $this->configuration['variant_settings'];
      }
      $form_state->set('variant', $variant_entity);
      $form_state->set('default_settings', $variant_settings);
      $form['variant_settings_wrapper']['variant_settings'] = $variant_entity->buildLayoutForm([], $form_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $variant = $form_state->getValue('variant');
    $this->configuration['variant'] = $variant;
    $variant_entity = NULL;
    if ($variant) {
      /** @var \Drupal\breezy_layouts\Entity\BreezyLayoutsVariantInterface $variant_entity */
      $variant_entity = $this->entityTypeManager->getStorage('breezy_layouts_variant')->load($variant);
    }
    if ($variant_entity) {
      $variant_settings = $form_state->getValue(['variant_settings_wrapper', 'variant_settings']);
      if ($variant_settings) {
        $this->configuration['variant_settings'] = $variant_settings;
        $classes = $variant_entity->buildLayoutClasses($variant_settings);
        $this->configuration['classes'] = $classes;
      }
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Variant select callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The portion of the form to return.
   */
  public function variantSelectCallback(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#parents'];
    array_splice($parents, -1, 1, 'variant_settings_wrapper');
    $variant_form = NestedArray::getValue($form, $parents);
    if (isset($variant_form)) {
      return $variant_form;
    }
    return [];
  }

}
