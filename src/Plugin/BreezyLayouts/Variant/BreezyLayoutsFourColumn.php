<?php

namespace Drupal\breezy_layouts\Plugin\BreezyLayouts\Variant;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Breezy Layouts Four Column Layout Variant plugin.
 *
 * @BreezyLayoutsVariantPlugin(
 *   id = "breezy_four_column",
 *   label = @Translation("Breezy four column"),
 *   description = @Translation("Provides a variant plugin for Breezy four
 *   column layout"),
 *   layout = "breezy-four-column",
 *   layout_elements = {
 *     "container" = @Translation("Container"),
 *     "wrapper" = @Translation("Wrapper"),
 *     "left_outer" = @Translation("Left outer"),
 *     "left_inner" = @Translation("Left inner"),
 *     "right_inner" = @Translation("Right inner"),
 *     "right_outer" = @Translation("Right outer")
 *   }
 * )
 */
class BreezyLayoutsFourColumn extends BreezyLayoutsVariantPluginBase implements BreezyLayoutsVariantPluginInterface {

  /**
   * Constructs a new BreezyLayoutsOneColumn plugin object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager
   *   The breakpoint manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface $element_plugin_manager
   *   The element plugin manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BreakpointManagerInterface $breakpoint_manager, ConfigFactoryInterface $config_factory, BreezyUtilityElementPluginManagerInterface $element_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $breakpoint_manager, $config_factory, $element_plugin_manager);
    $this->configuration += $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager */
    $breakpoint_manager = $container->get('breakpoint.manager');
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $container->get('config.factory');
    /** @var \Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface $element_plugin_manager $element_plugin_manager */
    $element_plugin_manager = $container->get('plugin.manager.breezy_utility.element');
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $breakpoint_manager,
      $config_factory,
      $element_plugin_manager
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'breakpoint_group' => '',
      'breakpoints' => [],
    ] + parent::defaultConfiguration();
  }

}
