<?php

namespace Drupal\breezy_layouts\Plugin\BreezyLayouts\Variant;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Provides an interface for BreezyLayoutsVariant plugins.
 */
interface BreezyLayoutsVariantPluginInterface extends ConfigurableInterface, ContainerFactoryPluginInterface, PluginFormInterface {

  /**
   * Retrieves the plugin's label.
   *
   * @return string
   *   The plugin's human-readable and translated label.
   */
  public function label() : string;

  /**
   * Retrieves the plugin's description.
   *
   * @return string
   *   The plugin's translated description; or empty string if it has none.
   */
  public function getDescription() : string;

  /**
   * Retrieves the layout plugin.
   *
   * @return string
   *   The layout plugin name.
   */
  public function getLayoutId() : string;

  /**
   * Layouts HTML elements.
   *
   * @return array
   *   An array of HTML elements that can contain properties.
   */
  public function getLayoutElements() : array;

  /**
   * Layout form.
   *
   * @param array $form
   *   The layout form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The layout form.
   */
  public function layoutForm(array $form, FormStateInterface $form_state) : array;

  /**
   * Build layout classes.
   *
   * @param array $layout_settings
   *   The settings submitted from the layout configuration form.
   *
   * @return array
   *   An array of classes keyed by the element.
   */
  public function buildLayoutClasses(array $layout_settings) : array;

}
