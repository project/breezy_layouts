<?php

namespace Drupal\breezy_layouts\Plugin\BreezyLayouts\Variant;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\breezy_layouts\Entity\BreezyLayoutsVariantInterface;
use Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface;
use Drupal\breezy_utility\Utility\BreezyUtilityBreakpointHelper;
use Drupal\breezy_utility\Utility\BreezyUtilityDialogHelper;
use Drupal\breezy_utility\Utility\BreezyUtilityElementHelper;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;

/**
 * Provides a base variant plugin class.
 *
 * @package Drupal\breezy_layouts\Plugin\BreezyLayouts\Variant
 */
abstract class BreezyLayoutsVariantPluginBase extends PluginBase implements ContainerFactoryPluginInterface, BreezyLayoutsVariantPluginInterface {

  use DependencySerializationTrait;

  /**
   * Drupal\breakpoint\BreakpointManagerInterface definition.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected BreakpointManagerInterface $breakpointManager;

  /**
   * The parent config entity.
   *
   * @var string
   */
  protected string $parentEntity;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Breezy Utility settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $breezyUtilitySettings;

  /**
   * BreezyUtilityElementPluginManagerInterface definition.
   *
   * @var \Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface
   */
  protected BreezyUtilityElementPluginManagerInterface $elementPluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BreakpointManagerInterface $breakpoint_manager, ConfigFactoryInterface $config_factory, BreezyUtilityElementPluginManagerInterface $element_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->breakpointManager = $breakpoint_manager;
    $this->elementPluginManager = $element_plugin_manager;
    $this->configFactory = $config_factory;
    $this->breezyUtilitySettings = $config_factory->get('breezy_utility.settings');
    $this->configuration += $this->defaultConfiguration();
    if (array_key_exists('_entity', $configuration)) {
      $this->parentEntity = $configuration['_entity'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['has_layout_form' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function label() : string {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() : string {
    return $this->pluginDefinition['description'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutId() : string {
    return $this->pluginDefinition['layout'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutElements() : array {
    return $this->pluginDefinition['layout_elements'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $variant = $form_state->get('variant');
    $breakpoints_wrapper_id = 'breakpoints-wrapper';
    $layout_elements = $this->getLayoutElements();
    $breakpoint_group = $this->breezyUtilitySettings->get('breakpoint_group');

    if ($breakpoint_group) {
      $form_state->set('breakpoint_group', $breakpoint_group);
      $form['breakpoint_group'] = [
        '#type' => 'value',
        '#value' => $breakpoint_group,
      ];
    }

    $form['breakpoints'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Breakpoints'),
      '#prefix' => '<div id="' . $breakpoints_wrapper_id . '">',
      '#suffix' => '</div>',
    ];

    $breakpoint_group = $form_state->get('breakpoint_group');
    if ($form_state->getValue('breakpoint_group')) {
      $breakpoint_group = $form_state->getValue('breakpoint_group');
    }

    if (!empty($breakpoint_group)) {
      $breakpoint_group_breakpoints = $this->breakpointManager->getBreakpointsByGroup($breakpoint_group);

      foreach ($breakpoint_group_breakpoints as $breakpoint_name => $breakpoint) {
        $breakpoint_name = BreezyUtilityBreakpointHelper::getSanitizedBreakpointName($breakpoint_name);
        $breakpoint_wrapper_id = 'breakpoints-' . $breakpoint_name;
        $enabled = FALSE;
        if (isset($this->configuration['breakpoints'][$breakpoint_name]['enabled'])) {
          $enabled = $this->configuration['breakpoints'][$breakpoint_name]['enabled'];
        }
        $form['breakpoints'][$breakpoint_name] = [
          '#type' => 'details',
          '#title' => $breakpoint->getLabel(),
          '#tree' => TRUE,
          '#prefix' => '<div id="' . $breakpoint_wrapper_id . '">',
          '#suffix' => '</div>',
          '#open' => $enabled,
        ];

        $breakpoints_form_state = $form_state->get(['plugin_configuration', 'breakpoints']);
        if ($breakpoints_form_state && isset($breakpoints_form_state[$breakpoint_name]['enabled'])) {
          $enabled = $breakpoints_form_state[$breakpoint_name]['enabled'];
        }
        $form['breakpoints'][$breakpoint_name]['enabled'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable'),
          '#default_value' => $enabled,
          '#ajax' => [
            'wrapper' => $breakpoint_wrapper_id,
            'callback' => [$this, 'pluginCallback'],
          ],
          '#breakpoint_name' => $breakpoint_name,
        ];

        foreach ($layout_elements as $layout_element_key => $layout_element_label) {
          $form['breakpoints'][$breakpoint_name][$layout_element_key] = [
            '#type' => 'fieldset',
            '#title' => $layout_element_label,
            '#states' => [
              'visible' => [
                'input[name="plugin_configuration[breakpoints][' . $breakpoint_name . '][enabled]"]' => ['checked' => TRUE],
              ],
            ],
          ];

          // Parent key.
          $parent_array = [
            'breakpoints',
            $breakpoint_name,
            $layout_element_key,
            'properties',
          ];
          $properties = $this->getProperties($parent_array);

          // Display properties.
          $form['breakpoints'][$breakpoint_name][$layout_element_key]['properties'] = $this->buildPropertiesTable($parent_array, $properties);
          $form['breakpoints'][$breakpoint_name][$layout_element_key]['add_property'] = $this->addPropertyLink($variant, $parent_array);

        }
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('plugin_configuration');
    $this->configuration['breakpoint_group'] = $values['breakpoint_group'];
    $this->configuration['breakpoints'] = $values['breakpoints'];
  }

  /**
   * {@inheritdoc}
   */
  public function layoutForm(array $form, FormStateInterface $form_state) : array {
    // Settings from layout form input.
    $default_settings = $form_state->get('default_settings');
    $layout_elements = $this->getLayoutElements();
    $breakpoint_group = $this->configuration['breakpoint_group'];
    $breakpoint_groups_breakpoints = $this->breakpointManager->getBreakpointsByGroup($breakpoint_group);
    $breakpoints = NULL;
    if (isset($this->configuration['breakpoints'])) {
      $breakpoints = $this->configuration['breakpoints'];
    }

    if (!$breakpoints) {
      return $form;
    }

    $form['breakpoints'] = [
      '#type' => 'container',
    ];

    foreach ($breakpoints as $breakpoint_name => $breakpoint_settings) {
      if (!$breakpoint_settings['enabled']) {
        continue;
      }

      // If there are elements with a UI, set all containers to visible.
      $breakpoint_name_converted = BreezyUtilityBreakpointHelper::getOriginalBreakpointName($breakpoint_name);
      if (!isset($breakpoint_groups_breakpoints[$breakpoint_name_converted])) {
        continue;
      }

      $form['breakpoints'][$breakpoint_name] = [
        '#type' => 'container',
        '#title' => $breakpoint_groups_breakpoints[$breakpoint_name_converted]->getLabel(),
      ];

      $breakpoint_has_ui = FALSE;
      $prefix = $this->getPrefixForBreakpoint($breakpoint_name);

      foreach ($layout_elements as $layout_element_key => $layout_element_label) {
        if (!isset($breakpoint_settings[$layout_element_key]['properties']) || empty($breakpoint_settings[$layout_element_key]['properties'])) {
          continue;
        }

        $form['breakpoints'][$breakpoint_name][$layout_element_key] = [
          '#type' => 'container',
          '#title' => $layout_element_label,
        ];

        $element_has_ui = FALSE;
        foreach ($breakpoint_settings[$layout_element_key]['properties'] as $property_name => $property_values) {
          if (!isset($property_values['element']) || empty($property_values['element'])) {
            continue;
          }

          $property = $property_values;
          $element = $property_values['element'];
          $default_value = NULL;
          if ($this->elementHasUi($property)) {
            $element_has_ui = TRUE;
            if (isset($default_settings['breakpoints'][$breakpoint_name][$layout_element_key][$property_name])) {
              $default_value = $default_settings['breakpoints'][$breakpoint_name][$layout_element_key][$property_name];
            }
          }
          $form['breakpoints'][$breakpoint_name][$layout_element_key][$property_name] = $this->buildFormElement($element, $prefix, $default_value);
        }

        if ($element_has_ui) {
          // If there are any elements with a UI, make the container a fieldset.
          $breakpoint_has_ui = TRUE;
          $form['breakpoints'][$breakpoint_name][$layout_element_key]['#type'] = 'fieldset';
        }
      }

      if ($breakpoint_has_ui) {
        $form['breakpoints'][$breakpoint_name]['#type'] = 'details';
      }
    }

    return $form;
  }

  /**
   * Builds a form element.
   *
   * @param array $element_definition
   *   The element definition array.
   * @param string $prefix
   *   A prefix (for the breakpoint).
   * @param mixed $default_value
   *   The default value for the field.
   *
   * @return array
   *   A renderable element.
   *
   * @see \Drupal\breezy_utility\Utility\BreezyUtilityElementHelper
   */
  protected function buildFormElement(array $element_definition, string $prefix = '', $default_value = NULL) {
    return BreezyUtilityElementHelper::buildFormElement($element_definition, $prefix, $default_value);
  }

  /**
   * If the element has a UI.
   *
   * Used to control layout form container visibility.
   *
   * @param array $property
   *   The property array.
   *
   * @return bool
   *   TRUE if the element has a UI.
   */
  protected function elementHasUi(array $property) : bool {
    $element_plugin = $this->elementPluginManager->getElementInstance($property);
    if ($element_plugin) {
      return $element_plugin->hasUi();
    }
    return FALSE;
  }

  /**
   * Get prefix for breakpoint.
   *
   * @param string $breakpoint_name
   *   The breakpoint name.
   *
   * @return string
   *   The prefix set for the breakpoint name.
   */
  protected function getPrefixForBreakpoint(string $breakpoint_name) : string {
    $breakpoints = $this->breezyUtilitySettings->get('breakpoints');
    if (isset($breakpoints[$breakpoint_name]['prefix'])) {
      return $breakpoints[$breakpoint_name]['prefix'];
    }
    return '';
  }

  /**
   * Get properties from configuration.
   *
   * @param array $parent_key
   *   The parent key.
   *
   * @return array
   *   An array of properties for a given key.
   */
  public function getProperties(array $parent_key) : array {
    $configuration = $this->getConfiguration();
    $properties = NestedArray::getValue($configuration, $parent_key);
    if ($properties) {
      return $properties;
    }
    return [];
  }

  /**
   * Build properties table.
   *
   * @param array $parent_key
   *   An array of parents.
   * @param array $properties
   *   An array of properties.
   *
   * @return array
   *   An array representing a variant properties table.
   */
  public function buildPropertiesTable(array $parent_key, array $properties = []) : array {

    $rows = [];
    if (!empty($properties)) {
      $delta = count($properties);
      $properties = $this->getOrderableElements($properties);
      foreach ($properties as $key => $property) {
        $rows[$key] = $this->getPropertyRow($key, $property, $delta, $parent_key);
      }
    }
    $table = [
      '#type' => 'table',
      '#sort' => TRUE,
      '#header' => $this->getPropertiesTableHeader(),
      '#empty' => $this->t('Add CSS properties.'),
      '#attributes' => [
        'class' => ['breezy-properties-form'],
      ],
      '#tabledrag' => [
        [
          'action' => 'match',
          'relationship' => 'parent',
          'group' => 'row-parent-key',
          'source' => 'row-key',
          'hidden' => TRUE,
          'limit' => FALSE,
        ],
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'row-weight',
        ],
      ],
    ] + $rows;

    return $table;
  }

  /**
   * Build variant properties table header.
   *
   * @return array
   *   An array of table header items.
   */
  protected function getPropertiesTableHeader() : array {
    $header = [];
    $header['title'] = $this->t('Title');
    $header['key'] = $this->t('Key');
    $header['property'] = $this->t('CSS Property');
    $header['type'] = $this->t('Field type');
    $header['weight'] = $this->t('Weight');
    $header['parent'] = [
      'data' => $this->t('Parent'),
      'class' => ['tabledrag-hide'],
    ];
    $header['operations'] = $this->t('Operations');
    return $header;
  }

  /**
   * Get property row.
   *
   * Builds a row for the Variant plugin properties table.
   *
   * @param string $key
   *   The property key.
   * @param array $property
   *   The configured property.
   * @param int $delta
   *   The row weight.
   * @param array $parent_key
   *   The parent key array.
   *
   * @return array
   *   The property in a row format.
   */
  public function getPropertyRow(string $key, array $property, int $delta, array $parent_key) : array {

    $row = [];

    $title = $property['element']['title'] ?? 'missing';
    $type = $property['element']['type'] ?? 'missing';
    $property_name = $property['property'] ?? 'missing';

    $row_class = ['draggable'];

    $row['#attributes']['data-breezy-key'] = $key;
    $row['#attributes']['data-breezy-type'] = $type;

    $row['#attributes']['class'] = $row_class;

    $row['title'] = [
      '#markup' => $title,
    ];

    $row['key'] = [
      '#markup' => $key,
    ];

    $row['property'] = [
      '#markup' => $property_name,
      '#allowed_tags' => ['pre'],
    ];

    $row['type'] = [
      '#markup' => $type,
    ];

    $row['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight for @title', ['@title' => $title]),
      '#description' => $this->t('Weight determines the order of the element in layout form.'),
      '#title_display' => 'invisible',
      '#default_value' => $property['weight'] ?? 0,
      '#wrapper_attributes' => ['class' => ['breezy-tabledrag-hide']],
      '#attributes' => [
        'class' => ['row-weight'],
      ],
      '#delta' => $delta,
    ];

    $row['parent'] = [
      '#wrapper_attributes' => ['class' => ['breezy-tabledrag-hide', 'tabledrag-hide']],
    ];

    $key_parents = array_merge($parent_key, [$key, 'key']);

    $row['parent']['key'] = [
      '#parents' => $key_parents,
      '#type' => 'hidden',
      '#value' => $key,
      '#attributes' => [
        'class' => ['row-key'],
      ],
    ];

    $key_parents_parent = array_merge($parent_key, [$key, 'parent_key']);

    $row['parent']['parent_key'] = [
      '#parents' => $key_parents_parent,
      '#type' => 'hidden',
      '#default_value' => json_encode($key_parents_parent),
      '#attributes' => [
        'class' => ['row-parent-key'],
      ],
    ];

    $query = [
      'key' => $key,
      'property' => $property_name,
      'parent' => Json::encode($parent_key),
    ];

    $element_edit_url = Url::fromRoute('entity.breezy_layouts_ui.element.edit_form', [
      'breezy_layouts_variant' => $this->parentEntity,
      'type' => $type,
    ],
      [
        'query' => $query,
      ]);
    $element_copy_url = Url::fromRoute('entity.breezy_layouts_ui.property.copy', [
      'breezy_layouts_variant' => $this->parentEntity,
    ],
    [
      'query' => $query,
    ]);
    $element_delete_url = Url::fromRoute('entity.breezy_layouts_ui.element.delete_form', [
      'breezy_layouts_variant' => $this->parentEntity,
      'type' => $type,
    ],
      [
        'query' => $query,
      ]);
    $row['operations'] = [
      '#type' => 'operations',
      '#prefix' => '<div class="breezy-dropbutton">',
      '#suffix' => '</div>',
    ];
    $row['operations']['#links']['edit'] = [
      'title' => $this->t("Edit"),
      'url' => $element_edit_url,
      'attributes' => BreezyUtilityDialogHelper::getOffCanvasDialogAttributes(),
    ];
    $row['operations']['#links']['copy'] = [
      'title' => $this->t('Copy'),
      'url' => $element_copy_url,
      'attributes' => BreezyUtilityDialogHelper::getOffCanvasDialogAttributes(),
    ];
    $row['operations']['#links']['delete'] = [
      'title' => $this->t("Delete"),
      'url' => $element_delete_url,
      'attributes' => BreezyUtilityDialogHelper::getModalDialogAttributes(),
    ];

    return $row;
  }

  /**
   * Get Variant elements as an associative array of orderable elements.
   *
   * @param array $properties
   *   The properties array.
   *
   * @return array
   *   An associative array of orderable elements.
   */
  protected function getOrderableElements(array $properties) : array {
    $weights = [];
    foreach ($properties as $property_key => &$property) {
      if (!isset($weights[$property_key])) {
        $property['weight'] = $weights[$property_key] = 0;
      }
      else {
        $property['weight'] = ++$weights[$property_key];
      }
    }

    return $properties;
  }

  /**
   * Add property link.
   *
   * @param \Drupal\breezy_layouts\Entity\BreezyLayoutsVariantInterface $variant
   *   The variant entity.
   * @param array $parent_key
   *   The parent key.
   *
   * @return array
   *   The add property link.
   */
  protected function addPropertyLink(BreezyLayoutsVariantInterface $variant, array $parent_key) : array {
    $parent = Json::encode($parent_key);
    return [
      '#type' => 'link',
      '#title' => $this->t('Add property'),
      '#url' => Url::fromRoute('entity.breezy_layouts_ui.property.add', ['breezy_layouts_variant' => $variant->id()], ['query' => ['parent' => $parent]]),
      '#attributes' => BreezyUtilityDialogHelper::getModalDialogAttributes('normal', ['use-ajax']),
    ];
  }

  /**
   * Plugin callback.
   *
   * Callback when a breakpoint is enabled.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state object.
   *
   * @return array
   *   The plugin configuration portion of the form array.
   */
  public function pluginCallback(array $form, FormStateInterface $form_state) : array {
    $trigger = $form_state->getTriggeringElement();
    $breakpoint_name = $trigger['#breakpoint_name'];
    $input = $form_state->getUserInput();
    $plugin_configuration = $input['plugin_configuration'];
    $enabled = $plugin_configuration['breakpoints'][$breakpoint_name]['enabled'];
    if ($form_state->getFormObject() instanceof EntityFormInterface) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $form_state->getFormObject()->getEntity();
      $stored_configuration = $entity->getPluginConfiguration();
      NestedArray::setValue($stored_configuration, ['breakpoints', $breakpoint_name, 'enabled'], $enabled);
      $entity->set('plugin_configuration', $stored_configuration);
      $entity->save();
    }
    return $form['plugin_configuration']['breakpoints'][$breakpoint_name];
  }

  /**
   * Variant save.
   *
   * Merges $form_state with $plugin_configuration.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param array $form_values
   *   The form values.
   *
   * @return array
   *   The merged form values / configuration.
   *
   * @see \Drupal\breezy_layouts\Form\BreezyLayoutsVariantForm
   */
  public function mergeFormState(array $configuration, array $form_values) {
    $breakpoints = $configuration['breakpoints'];
    $config_values = [];
    foreach ($breakpoints as $breakpoint_name => $breakpoint_settings) {
      if (!$breakpoint_settings['enabled']) {
        continue;
      }
      // Get $properties (fields) from $config.
      $config_values[$breakpoint_name] = $breakpoint_settings;
    }

    return NestedArray::mergeDeepArray($form_values['breakpoints'], $config_values);
  }

  /**
   * {@inheritdoc}
   */
  public function buildLayoutClasses(array $layout_settings) : array {
    $classes = [];
    if (empty($layout_settings) || !isset($layout_settings['breakpoints'])) {
      return $classes;
    }
    foreach ($layout_settings['breakpoints'] as $breakpoint_name => $breakpoint_settings) {

      $prefix = $this->getPrefixForBreakpoint($breakpoint_name);
      foreach ($breakpoint_settings as $element_name => $element_settings) {
        foreach ($element_settings as $key => $value) {
          if (is_array($value)) {
            // Checkboxes can have arrays of classes.
            $classes[$element_name][] = implode(' ' . $prefix, $value);
          }
          else {
            $classes[$element_name][] = $prefix . $value;
          }
        }
      }
    }
    return $classes;
  }

}
