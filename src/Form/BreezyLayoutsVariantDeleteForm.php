<?php

namespace Drupal\breezy_layouts\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the form to delete a BreezyLayoutsVariant config entity.
 */
class BreezyLayoutsVariantDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %label', ['%label' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.breezy_layouts_variant.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Load the entity to be deleted.
    $entity = $this->entity;

    // Delete the entity.
    $entity->delete();

    // Display a message indicating successful deletion.
    $this->messenger()->addStatus($this->t('The variant has been deleted.'));

    // Redirect the user to the collection page of Breezy Layouts variants.
    $form_state->setRedirect('entity.breezy_layouts_variant.collection');
  }

}
