<?php

namespace Drupal\breezy_layouts\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Breezy Layout Variant Plugin.
 *
 * @Annotation
 */
class BreezyLayoutsVariantPlugin extends Plugin {

  /**
   * The plugin label.
   *
   * @var string
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The plugin description.
   *
   * @var string
   *
   * @ingroup plugin_translatable
   */
  public string $description;

  /**
   * The layout plugin.
   *
   * @var string
   */
  public string $layout;

  /**
   * Layouts HTML elements.
   *
   * Elements of the layout that can receive CSS classes.
   *
   * @var array
   */
  public array $layout_elements = [];

}
