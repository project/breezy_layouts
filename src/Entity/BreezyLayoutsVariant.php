<?php

namespace Drupal\breezy_layouts\Entity;

use Drupal\breezy_utility\Utility\BreezyUtilityElementHelper;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the BreezyLayoutsVariant config entity.
 *
 * @ConfigEntityType(
 *   id = "breezy_layouts_variant",
 *   label = @Translation("Breezy Layouts Variant"),
 *   label_collection = @Translation("Breezy Layouts Variants"),
 *   label_plural = @Translation("Breezy Layouts Variants"),
 *   label_count = @PluralTranslation(
 *      singular = "@count variant",
 *      plural = "@count variants",
 *    ),
 *   admin_permission = "administer breezy layout variants",
 *   config_prefix = "breezy_layouts_variant",
 *   handlers = {
 *      "route_provider" = {
 *        "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *      },
 *     "storage" = "Drupal\breezy_layouts\Storage\BreezyLayoutsVariantStorage",
 *     "list_builder" = "Drupal\breezy_layouts\BreezyLayoutsVariantListBuilder",
 *     "form" = {
 *       "add" = "Drupal\breezy_layouts\Form\BreezyLayoutsVariantForm",
 *       "edit" = "Drupal\breezy_layouts\Form\BreezyLayoutsVariantForm",
 *       "duplicate" = "Drupal\breezy_layouts\Form\BreezyLayoutsDuplicateForm",
 *       "delete" = "Drupal\breezy_layouts\Form\BreezyLayoutsVariantDeleteForm",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "status",
 *     "layout",
 *     "plugin_id",
 *     "plugin_configuration",
 *   },
 *   links = {
 *     "collection" = "/admin/config/content/breezy/layouts/variants",
 *     "edit-form" = "/admin/config/content/breezy/layouts/variants/{breezy_layouts_variant}",
 *     "duplicate-form" = "/admin/config/content/breezy/layouts/variants/{breezy_layouts_variant}/duplicate",
 *     "delete-form" = "/admin/config/content/breezy/layouts/variants/{breezy_layouts_variant}/delete"
 *   }
 * )
 */
class BreezyLayoutsVariant extends ConfigEntityBase implements BreezyLayoutsVariantInterface {

  /**
   * The variant id.
   *
   * @var string
   */
  protected string $id;

  /**
   * The variant label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The variant status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The layout the variant applies to.
   *
   * @var string
   */
  protected string $layout;

  /**
   * The breakpoint group.
   *
   * @var string
   */
  protected string $breakpointGroup = '';

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected string $plugin_id = '';

  /**
   * The plugin configuration.
   *
   * @var array
   */
  protected array $plugin_configuration = [];

  /**
   * {@inheritdoc}
   */
  public function getLayout(): string {
    return $this->layout;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return $this->plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginId($plugin_id): BreezyLayoutsVariant {
    $this->plugin_id = $plugin_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfiguration() {
    return $this->plugin_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginConfiguration(array $configuration): BreezyLayoutsVariant {
    $this->plugin_configuration = $configuration;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled($enabled): BreezyLayoutsVariant {
    $this->status = $enabled;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive(): bool {
    if (!$this->isEnabled()) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setBreakpointGroup(string $breakpoint_group): BreezyLayoutsVariant {
    $this->breakpointGroup = $breakpoint_group;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBreakpointGroup(): string {
    return $this->breakpointGroup;
  }

  /**
   * Get element configuration.
   *
   * @param array $parent_key
   *   The element parent key.
   * @param mixed $key
   *   The element form key.
   *
   * @return array|null
   *   An array containing an initialized element.
   */
  public function getElementConfiguration(array $parent_key, string $key) {
    // Find the element configuration based on the key.
    $variant_plugin_configuration = $this->getPluginConfiguration();
    $existing_properties = NestedArray::getValue($variant_plugin_configuration, $parent_key);

    return $existing_properties[$key] ?? NULL;

  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledProperties() : array {
    $plugin_configuration = $this->getPluginConfiguration();
    $enabled_properties = [];
    if (!isset($plugin_configuration['breakpoints'])) {
      return $enabled_properties;
    }
    foreach ($plugin_configuration['breakpoints'] as $breakpoint_name => $breakpoint_sections) {
      if ($breakpoint_sections['enabled']) {
        $enabled_properties['breakpoints'][$breakpoint_name] = $breakpoint_sections;
        unset($enabled_properties['breakpoints'][$breakpoint_name]['enabled']);
      }
    }
    return $enabled_properties;

  }

  /**
   * {@inheritdoc}
   */
  public function setElementProperties(string $key, array $properties, array $parent_key = []): BreezyLayoutsVariant {

    if (empty($parent_key)) {
      return $this;
    }
    // Get variant plugin, determine where the $key and $parent_key goes, inject
    // the new element, set its properties.
    $plugin_configuration = $this->getPluginConfiguration();
    if (!$plugin_configuration) {
      $plugin_configuration = [];
    }
    $existing_properties = NestedArray::getValue($plugin_configuration, $parent_key);
    if (is_array($existing_properties)) {
      $existing_properties[$key] = $properties;
    }
    else {
      $existing_properties = [$key => $properties];
    }
    NestedArray::setValue($plugin_configuration, $parent_key, $existing_properties);
    $this->setPluginConfiguration($plugin_configuration);
    return $this;
  }

  /**
   * Set element properties.
   *
   * @param array $elements
   *   An associative nested array of elements.
   * @param string $key
   *   The element's key.
   * @param array $properties
   *   An associative array of properties.
   * @param array $parent_key
   *   (optional) The element's parent key. Only used for new elements.
   *
   * @return bool
   *   TRUE when the element's properties has been set. FALSE when the element
   *   has not been found.
   */
  protected function setElementPropertiesRecursive(array &$elements, $key, array $properties, array $parent_key = []) {
    foreach ($parent_key as $parent_array_key) {
      // @todo Remember why this was here.
      if (array_key_exists($parent_array_key, $elements)) {

      }
    }
    foreach ($elements as $element_key => &$element) {
      // Make sure the element key is a string.
      $element_key = (string) $element_key;

      if (!BreezyUtilityElementHelper::isElement($element, $element_key)) {
        continue;
      }

      if ($element_key === $key) {
        $element = $properties + BreezyUtilityElementHelper::removeProperties($element);
        return TRUE;
      }

      if ($element_key === $parent_key) {
        $element[$key] = $properties;
        return TRUE;
      }

      if ($this->setElementPropertiesRecursive($element, $key, $properties, $parent_key)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Delete element.
   *
   * @param string $key
   *   The element key.
   * @param array $parent_key
   *   The element parent.
   */
  public function deleteElement($key, array $parent_key) {
    array_push($parent_key, $key);
    $configuration = $this->getPluginConfiguration();
    NestedArray::unsetValue($configuration, $parent_key, $key);
    $this->setPluginConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function buildLayoutForm($form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    $variant_configuration = $this->getPluginConfiguration();
    if (empty($variant_configuration)) {
      return $form;
    }

    /** @var \Drupal\breezy_layouts\Service\BreezyLayoutsVariantPluginManagerInterface $variant_manager */
    $variant_manager = \Drupal::service('plugin.manager.breezy_layouts.variant');

    $variant_plugin = $variant_manager->createInstance($this->getPluginId(), $variant_configuration);

    $form_state->set('breakpoint_group', $this->getBreakpointGroup());
    $form = $variant_plugin->layoutForm($form, $form_state);
    return $form;
  }

  /**
   * Build layout classes.
   *
   * @param array $layout_form_settings
   *   The settings submitted from the layout form.
   *
   * @return array
   *   An array of classes keyed by the element.
   */
  public function buildLayoutClasses(array $layout_form_settings) {
    $variant_configuration = $this->getPluginConfiguration();
    if (empty($variant_configuration)) {
      return [];
    }
    /** @var \Drupal\breezy_layouts\Service\BreezyLayoutsVariantPluginManagerInterface $variant_manager */
    $variant_manager = \Drupal::service('plugin.manager.breezy_layouts.variant');
    $variant_plugin = $variant_manager->createInstance($this->getPluginId(), $variant_configuration);
    if (!$variant_plugin) {
      return [];
    }
    return $variant_plugin->buildLayoutClasses($layout_form_settings);
  }

}
