<?php

namespace Drupal\breezy_layouts\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an interface for defining Breezy Layout Variant entities.
 */
interface BreezyLayoutsVariantInterface extends ConfigEntityInterface {

  /**
   * Get variant layout.
   *
   * @return string
   *   The layout id.
   */
  public function getLayout(): string;

  /**
   * Get plugin id.
   *
   * @return string
   *   The plugin id.
   */
  public function getPluginId(): string;

  /**
   * Set plugin id.
   *
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return \Drupal\breezy_layouts\Entity\BreezyLayoutsVariant
   *   The variant object.
   */
  public function setPluginId($plugin_id): BreezyLayoutsVariant;

  /**
   * Get the BreezyLayoutsVariant plugin configuration.
   *
   * @return mixed
   *   The plugin configuration.
   */
  public function getPluginConfiguration();

  /**
   * Set the BreezyLayoutsVariant plugin configuration.
   *
   * @param array $configuration
   *   The plugin configuration array.
   *
   * @return \Drupal\breezy_layouts\Entity\BreezyLayoutsVariant
   *   The variant object.
   */
  public function setPluginConfiguration(array $configuration): BreezyLayoutsVariant;

  /**
   * Is the variant enabled.
   *
   * @return bool
   *   True if enabled.
   */
  public function isEnabled(): bool;

  /**
   * Set the variant enabled.
   *
   * @return \Drupal\breezy_layouts\Entity\BreezyLayoutsVariant
   *   The variant object.
   */
  public function setEnabled($enabled): BreezyLayoutsVariant;

  /**
   * Is active.
   *
   * @return bool
   *   Whether the variant is active.
   */
  public function isActive(): bool;

  /**
   * Set the breakpoint group.
   *
   * @param string $breakpoint_group
   *   The breakpoint group name.
   *
   * @return \Drupal\breezy_layouts\Entity\BreezyLayoutsVariant
   *   The variant object.
   */
  public function setBreakpointGroup(string $breakpoint_group): BreezyLayoutsVariant;

  /**
   * Get the breakpoint group.
   *
   * @return string
   *   The breakpoint group name.
   */
  public function getBreakpointGroup(): string;

  /**
   * Get enabled properties.
   *
   * @return array
   *   An array of enabled properties, keyed by breakpoint.
   */
  public function getEnabledProperties() : array;

  /**
   * Set element properties.
   *
   * @param string $key
   *   The element key.
   * @param array $properties
   *   The element properties.
   * @param array $parent_key
   *   The parent key.
   *
   * @return \Drupal\breezy_layouts\Entity\BreezyLayoutsVariant
   *   The BreezyLayoutsVariant entity.
   */
  public function setElementProperties(string $key, array $properties, array $parent_key = []): BreezyLayoutsVariant;

  /**
   * Build layout form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   An array of form elements.
   */
  public function buildLayoutForm($form, FormStateInterface $form_state);

}
